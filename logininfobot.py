import os, pwd, tweepy
from time import localtime, strftime


def main():
    timenows = strftime("%a, %d %b %Y %H:%M:%S", localtime())
    hostnm = os.uname()
    curruser = pwd.getpwuid( os.getuid() )[ 0 ]
    tweets = "@robeevanjava user: %s login to %s on %s" %(curruser,hostnm[1],timenows)
    
    CONSUMER_KEY = '#'
    CONSUMER_SECRET = '#'
    ACCESS_KEY = '#'
    ACCESS_SECRET = '#'

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
    api = tweepy.API(auth)
    api.update_status(tweets)
    
if __name__ == '__main__':
    main()

