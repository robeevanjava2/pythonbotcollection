import xmpp
import sys

NOTIFICATION_SERVER = "dmz"
NOTIFICATION_AGENT_NAME = "test.bot"
NOTIFICATION_AGENT_PASSWORD = ""

def kirim( account, fullname, notifications ):
    recipientAddress = "%s@%s" % ( account, NOTIFICATION_SERVER )
    messageHead = "Hi %s,\n" % fullname
    messageBody = messageHead + notifications
    
    message = xmpp.protocol.Message( recipientAddress, messageBody )
    message.setAttr('type', 'chat')
    
    jid = xmpp.protocol.JID(
        "%s@%s" % ( NOTIFICATION_AGENT_NAME, NOTIFICATION_SERVER )
        )
    
    client = xmpp.Client( jid.getDomain(), debug = [] )
    
    if client.connect() == "":
        return False
    
    if client.auth( jid.getNode(), NOTIFICATION_AGENT_PASSWORD ) == None:
        return False
    
    client.send( message )
    client.disconnect()
    
    return True

def kirimgtalk():
    login = 'milis.van.java' # @gmail.com
    pwd   = ''
    
    cnx = xmpp.Client('gmail.com')
    cnx.connect( server=('talk.google.com',5223) )
    
    cnx.auth(login,pwd, 'botty')
    
    cnx.send( xmpp.Message( "dedi.pambudi@gmail.com" ,"Hello World form Python" ) )
    
def main():    
    notifications = str(sys.argv[1])
    kirimgtalk()
    #kirim('anggarda.tiratana','Anggarda',notifications)
    #kirim('dedi.pambudi','Dedi Pambudi',notifications)
    #kirim('daniel.tobing','Daniel Tobing',notifications)
    
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        quit()

