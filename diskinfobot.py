
import win32com.client as com
import win32api
import tweepy
from time import gmtime, strftime


def TotalSize(drive):
    try:
        fso = com.Dispatch("Scripting.FileSystemObject")
        drv = fso.GetDrive(drive)
        return drv.TotalSize/2**30
    except:
        return 0

def FreeSpace(drive):
    try:
        fso = com.Dispatch("Scripting.FileSystemObject")
        drv = fso.GetDrive(drive)
        return drv.FreeSpace/2**30
    except:
        return 0

def main():
   
    drive_c = r'c'
    drive_c_total = TotalSize(drive_c)
    drive_c_free = FreeSpace(drive_c)
    userlogged = win32api.GetUserName()
    timenows = strftime("%a, %d %b %Y %H:%M:%S", gmtime())
    

    tweet = "@robeevanjava Logged User : %s . At : %s " %(userlogged, timenows)
    tweets = "@robeevanjava DRIVE C: %d GB. FREE : %d GB" %(drive_c_total,drive_c_free)
   
    CONSUMER_KEY = '#'
    CONSUMER_SECRET = '#'
    ACCESS_KEY = '#'
    ACCESS_SECRET = '#'

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
    api = tweepy.API(auth)
    api.update_status(tweet)
    api.update_status(tweets)
    
if __name__ == '__main__':
    main()

