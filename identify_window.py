from __future__ import with_statement
from contextlib import contextmanager
import os
import sys
from xml.etree import ElementTree
import sqlite3
import psycopg2
import traceback

PG_CONNECTION_PARAMS = {
    "hostaddr": "192.168.45.163",
    "port": "5432",
    "dbname": "sw_usage",
    "user": "postgres",
    "password": ""
    }
'''
test
'''
#===============================================================================
# 
# 
# 
#===============================================================================
@contextmanager
def pgConnect( **kwargs ):
    
    paramStr = " ".join( [ "%s=%s" % ( key, value ) for key, value in PG_CONNECTION_PARAMS.iteritems()] )
    connection = psycopg2.connect( paramStr )
    
    try:
        yield connection
    finally:
        connection.close()


@contextmanager
def pgTransaction( connection ):
    cursor = connection.cursor()
    try:
        yield cursor
    except:
        cursor.close()
        connection.rollback()
        raise
    cursor.close()
    connection.commit()


@contextmanager
def pgQuery( connection, query, params = () ):
    cursor = connection.cursor()
    try:
        cursor.execute( query, params )
        yield cursor
    finally:
        cursor.close()

@contextmanager
def sqliteConnection( path = "C:/robi/system/localsession.db" ):
    
    connection = sqlite3.connect(
        path,
        detect_types = sqlite3.PARSE_DECLTYPES
        )
    
    try:
        yield connection
    finally:
        connection.close()


@contextmanager
def sqliteTransaction( connection ):
    
    cursor = connection.cursor()
    
    try:
        yield cursor
    except:
        cursor.close()
        connection.rollback()
        raise
    
    cursor.close()
    connection.commit()



def main():
    
    with sqliteConnection() as conn:
        conn.text_factory = str
        with sqliteTransaction (conn) as cursor:
            cursor.execute("SELECT DISTINCT class_name, caption_text, module FROM window")
            for litedata in cursor:
                with pgConnect( **PG_CONNECTION_PARAMS ) as connection:
                    connection.set_client_encoding('LATIN9')
                    with pgTransaction( connection ) as cursors:
                        cursors.execute( "SELECT identify_application_window( %s, %s, %s )", ( litedata[0], litedata[1], litedata[2] ) )
                        values = cursors.fetchone()
                        print str(litedata[0])+" | "+str(litedata[1])+" | "+str(litedata[2])+" | "+str(values[0])
                
    


if __name__ == "__main__":
    main()


